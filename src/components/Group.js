import React from 'react';
import Navigation from './Navigation';
import Sidebar from './Sidebar';
import Groups from './group/Groups';



export default function Group() {
    return (
    <div id="page-container">
        <Navigation />
        <Sidebar />
        <Groups /> 

        
      </div>
    )
}
