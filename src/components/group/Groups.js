import React, { useEffect } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { useSelector, useDispatch } from "react-redux";

import { fetchAuthPosts } from '../../store/modules/posts/actions/postsAction';

import Sidebar from '../Sidebar.js';
import '../posts/Posts.css';
import Group from './Group';


const Preference = () => {

  const currentState = useSelector((state) => state.Auth);
  const authID = currentState.currentUser.id

  const dispatch = useDispatch();

  const getAuthPosts = id => dispatch(fetchAuthPosts(id));

  useEffect(() => {
    getAuthPosts(authID);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

   //incase someone visits the route manually
  if(!currentState.isAuthenticated){
    return <Redirect to='/login' />
  }
    

  return (
	<>
      <Sidebar />
    <div className="content">
      <div className="container card">
        <h3 className="text-center mt-3"><b>Group List</b></h3>
            <hr />
            <div className="container">
                <div  className="mt-2 style-card" >
                    <Link to={'/groups/2' } >
                         <Group/>
                    </Link>
                </div>
            </div>
        </div>
    </div>
    </>
  );
}

export default Preference
