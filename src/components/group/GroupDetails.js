import React from "react";
import { Button, Card, CardText, CardBody, CardTitle } from "reactstrap";
import Navigation from "../Navigation";
import Sidebar from './../Sidebar.js';



const GroupDetails = () => {

    return (<>

    <Navigation />
    <Sidebar />

    <div className="content">
        <div className="container card">
            <h3 className="text-center mt-3"><b>Detail Group #1</b></h3>
                <hr />
                <div className="container">
                    <div  className="mt-2 style-card" >
                        <Card>
                            <CardBody>
                            <CardTitle className=""></CardTitle><span style={{ float: "right" }}><Button variant="light">Lihat Member</Button></span>
                                    <CardTitle className=""><h6>Jumlah Anggota : 203</h6></CardTitle>
                                    <CardTitle className=""><h6>Deskripsi Group :</h6></CardTitle>
                                    <CardText>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</CardText>
                            </CardBody>
                        </Card>

                        <Card>
                                <CardBody>                
                                    <CardTitle className=""><h4>Projek #1</h4></CardTitle>
                                    <CardTitle className=""><h6>Deskripsi Projek :</h6></CardTitle>
                                    <CardText>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</CardText>
                                    {/* <CardText>{post.tag}</CardText> */}    
                                    <Card>
                                        <CardBody className="style-card-body">                
                                            <CardTitle className="">
                                                <span><b>Tugas #1</b></span> 
                                                <span style={{ float: "right" }}></span>
                                            </CardTitle>                                        
                                        </CardBody>            
                                    </Card>  

                                    <Card>
                                        <CardBody className="style-card-body">                
                                            <CardTitle className="">
                                                <span><b>Tugas #2</b></span> 
                                                <span style={{ float: "right" }}></span>
                                            </CardTitle>                                        
                                        </CardBody>            
                                    </Card> 

                                    <Card>
                                        <CardBody className="style-card-body">                
                                            <CardTitle className="">
                                                <span><b>Tugas #3</b></span> 
                                                <span style={{ float: "right" }}></span>
                                            </CardTitle>                                        
                                        </CardBody>            
                                    </Card>  

                                                           
                                </CardBody>   
                                <Button variant="light">Tambah Tugas</Button>         
                        </Card><br/>

                        <Card>
                                <CardBody>                
                                    <CardTitle className=""><h4>Projek #2</h4></CardTitle>
                                    <CardTitle className=""><h6>Deskripsi Projek :</h6></CardTitle>
                                    <CardText>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</CardText>
                                    {/* <CardText>{post.tag}</CardText> */}    
                                    <Card>
                                        <CardBody className="style-card-body">                
                                            <CardTitle className="">
                                                <span><b>Tugas #1</b></span> 
                                                <span style={{ float: "right" }}></span>
                                            </CardTitle>                                        
                                        </CardBody>            
                                    </Card>  

                                    <Card>
                                        <CardBody className="style-card-body">                
                                            <CardTitle className="">
                                                <span><b>Tugas #2</b></span> 
                                                <span style={{ float: "right" }}></span>
                                            </CardTitle>                                        
                                        </CardBody>            
                                    </Card> 

                                    <Card>
                                        <CardBody className="style-card-body">                
                                            <CardTitle className="">
                                                <span><b>Tugas #3</b></span> 
                                                <span style={{ float: "right" }}></span>
                                            </CardTitle>                                        
                                        </CardBody>            
                                    </Card>  

                                                           
                                </CardBody>   
                                <Button variant="light">Tambah Tugas</Button>         
                        </Card>  
                        
                        <span style={{ float: "right" }}><Button variant="light">Tambah Projek</Button></span>
                    </div>
                </div>
        </div>
    </div>
       
       
        
        </>

        
    );


};

export default GroupDetails;
