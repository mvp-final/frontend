import React, { useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom'
import { useSelector, useDispatch } from "react-redux";
import { FaFilter } from 'react-icons/fa';
import Default from "../../Assets/default.png";

import { fetchAuthPosts } from '../../store/modules/posts/actions/postsAction';
import { CardBody, CardTitle } from "reactstrap";
import AuthPost from './AuthPost'
import Navigation from '../Navigation'
import Sidebar from './../Sidebar.js';
import './Posts.css';


const Trending = () => {

  const currentState = useSelector((state) => state.Auth);
  const authID = currentState.currentUser.id

  const postsSelector = useSelector((state) => state.PostsState);
  const dispatch = useDispatch();

  const getAuthPosts = id => dispatch(fetchAuthPosts(id));

  useEffect(() => {
    getAuthPosts(authID);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

   //incase someone visits the route manually
  if(!currentState.isAuthenticated){
    return <Redirect to='/login' />
  }

  let authPosts = postsSelector.authPosts.map(post => {
    return (
      <div  className="mt-2 style-card" key={post.id}>
       <Link to={'/posts/' + post.id} key={post.id}>
        <AuthPost post={post} key={post.id} />
      </Link>
    </div>
      );
  })
    

  return (
    <div>
        <Navigation />
      <Sidebar />
      <div className="content">
      <div className="container card">
      <h3 className="text-center mt-3"><b>Trending</b></h3>
      <hr />

      <div className="container">
            <CardBody className="style-card-body">
                
                <div className="section"><h4>Now on trend</h4></div>
                { authPosts.length > 0 ? (
          <div className="container">{authPosts}</div>
        ) : ( 
          <div className="text-center mt-4">
            <div style={{fontSize: "100px"}}><FaFilter /></div>
            <p className="mt-2">Wait we are fetching data for you</p>
            <div className="mt-4">
              <Link to="/createpost" className="btn btn-primary">Create Post</Link>
            </div>
          </div>
        
        )}
                
            </CardBody>

        </div>

        <div className="container">
                
                <div className="section"><h4>Contributors</h4></div>
                <CardBody className="style-card-body ml-5 mr-5">
                <CardTitle>

          <span>
                        <span className="mr-2"><img className="img_style_post" src={Default} alt="no one 2" /></span>
                        <span href="" style={{ fontWeight: "bold" }}>
                            Nana
                        </span>
                    </span>
        
          </CardTitle>
                
            </CardBody>

        </div>
        
      </div>
    </div>
    </div>
  );
}

export default Trending