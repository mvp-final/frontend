import React, { useEffect } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { useSelector, useDispatch } from "react-redux";
import { FaFilter } from 'react-icons/fa'

import { fetchAuthPosts } from '../../store/modules/posts/actions/postsAction';
import AuthPost from './AuthPost'
import Navigation from '../Navigation'
import Sidebar from '../Sidebar.js';
import './Posts.css';


const Preference = () => {

  const currentState = useSelector((state) => state.Auth);
  const authID = currentState.currentUser.id

  const postsSelector = useSelector((state) => state.PostsState);
  const dispatch = useDispatch();

  const getAuthPosts = id => dispatch(fetchAuthPosts(id));

  useEffect(() => {
    getAuthPosts(authID);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

   //incase someone visits the route manually
  if(!currentState.isAuthenticated){
    return <Redirect to='/login' />
  }

  let authPosts = postsSelector.authPosts.map(post => {
    return (
      <div  className="mt-2 style-card" key={post.id}>
       <Link to={'/posts/' + post.id} key={post.id}>
        <AuthPost post={post} key={post.id} />
      </Link>
    </div>
      );
  })
    

  return (
    <div className="page-container">
        <Navigation />
      <Sidebar />
      <div className="content">
      <div className="container card">
      <h3 className="text-center mt-3"><b>Preference</b></h3>
      <hr />
      <div className="text-center">
          <button className="btn1" >
           				 Life Style
          			</button>{" "}

          <button className="btn1" >
           				 Sport
          			</button>{" "}

          <button className="btn1" >
           				 Science
          			</button>
      </div>

        { authPosts.length > 0 ? (
          <div className="container">{authPosts}</div>
        ) : ( 
          <div className="text-center mt-4">
            <div style={{fontSize: "100px"}}><FaFilter /></div>
            <p className="mt-2">We are fetching data for you</p>
            <div className="mt-4">
              <Link to="/createpost" className="btn btn-primary">Create Post</Link>
            </div>
          </div>
        
        )}
      </div>
    </div>
    </div>
  );
}

export default Preference
