import React, { useState } from "react";
import { Label, Input, FormGroup, Button, Card, CardHeader, CardBody } from "reactstrap";
import { Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from "react-redux";

import "./Posts.css";
import Navigation from '../Navigation'
import Sidebar from './../Sidebar.js';
import { createPost } from '../../store/modules/posts/actions/postsAction';



const CreatePost = () => {

  const currentState = useSelector((state) => state);

  const [post, setPost] = useState({
    title:'',
    content: '',
    // tag:'',
  });
  const dispatch = useDispatch()

  const addPost = (postDetails) => dispatch(createPost(postDetails))

  const handleChange = e => {
    setPost({
      ...post,
      [e.target.name]: e.target.value
    })
  }
  const submitUser = (e) => {
    e.preventDefault()
    addPost({
      title: post.title,
      content: post.content,
      // tag: post.tag,
    });
  }

  if(!currentState.Auth.isAuthenticated){
    return <Redirect to='/login' />
  }
    return (
      <div>
        <div>
          <Navigation />
        </div>
        <Sidebar />
        <div className="post-style">
        <Card className="card-style">
          <CardHeader><h5><b>Create Post</b></h5></CardHeader>
          <CardBody>
          <form onSubmit={submitUser}>
          <FormGroup>
            <Label>Title</Label>
            <Input type="text" name="title" placeholder="Enter title"  onChange={handleChange}/>
            { currentState.PostsState.postsError && currentState.PostsState.postsError.Required_title ? (
              <small className="color-red">{currentState.PostsState.postsError.Required_title}</small>
              ) : (
                ""
              )}
              { currentState.PostsState.postsError && currentState.PostsState.postsError.Taken_title ? (
              <small className="color-red">{ currentState.PostsState.postsError.Taken_title }</small>
              ) : (
                ""
              )}
          </FormGroup>
          
          <FormGroup>
            <Label>Content</Label>
            <Input type="textarea" cols="30" rows="6" name="content" id="" placeholder="Enter description" onChange={handleChange} />
            { currentState.PostsState.postsError && currentState.PostsState.postsError.Required_content ? (
              <small className="color-red">{currentState.PostsState.postsError.Required_content}</small>
              ) : (
                ""
              )}
            </FormGroup>

            {/* <FormGroup>
            <Label>Tags</Label>
            <Input type="text" cols="30" rows="6" name="tag" id="" placeholder="tags" onChange={handleChange} />
            { currentState.PostsState.postsError && currentState.PostsState.postsError.Required_tag ? (
              <small className="color-red">{currentState.PostsState.postsError.Required_tag}</small>
              ) : (
                ""
              )}
            </FormGroup> */}


            { currentState.PostsState.isLoading ? (
              <Button
                color="primary"
                type="submit"
                block
                disabled
              >
                Creating...
            </Button>
            ) : (
              <Button
                color="primary"
                type="submit"
                block
              >
              Create Post
            </Button>
            )}
            </form>
            </CardBody>
          </Card>
        </div>
        </div>
    );
}

export default CreatePost
