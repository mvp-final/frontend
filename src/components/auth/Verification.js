import React from 'react';
import img from '../../Assets/verifikasi.jpg';

export default function Verification() {
    return (
        <div>
           
            <section className="section text-center">
                <div className="container mt-5 bg-white">
                    <div className="page-error">
                        <div className="page-inner">
                            <div className="page-description pb-5">
                                <h5>Terimakasih telah mendaftar!</h5>
                                <h4>Akun Anda Sudah Terdaftar pada CoCreate<br/>Silakan Cek Email Untuk Verifikasi</h4>
                            </div>
                            <img src={img} width="50%" alt="Verification"></img>
                            <br/><br/>
                            <a href="/login"><button className="btn btn-primary">
                                Login
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        
        </div>
    )
}
