import React from "react";
import { Card, CardHeader, CardBody } from "reactstrap";
import Navigation from '../Navigation';
import Sidebar from '../Sidebar.js';

function Interest() {
    return (
        <div id="app">
            <Navigation />
            <Sidebar />
            <div className="post-style">
            
            <Card className="card-style">
                <CardHeader><h5><b>Preference Setting</b></h5></CardHeader>
            <CardBody>
                <h6>Please choose one of categories below to set up your preference content</h6>
                            <br/>

                            <form>
                                <div className="form-group">
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox" />
                                        <label className="form-check-label">
                                            Sport
                                        </label>
                                    </div>                                    
                                </div>
                                <div className="form-group">
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox" />
                                        <label className="form-check-label">
                                            Science
                                        </label>
                                    </div>                                    
                                </div>
                                <div className="form-group">
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox" />
                                        <label className="form-check-label">
                                            Life Style
                                        </label>
                                    </div>                                    
                                </div>
                                <div className="form-group">
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox" id="defaultCheck1" />
                                        <label className="form-check-label">
                                            Technology
                                        </label>
                                    </div>                                    
                                </div>
                        </form>
            </CardBody>

            <div className="card-footer">
                            <a type="submit" href="/profile/:id" className="btn btn-primary">Back</a>
                            <a type="submit" href="/profile/:id" className="btn btn-primary ml-5">Submit</a>
                        </div>

          </Card>
        </div>
    </div>
    )
}

export default Interest